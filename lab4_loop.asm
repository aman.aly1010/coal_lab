.model small
.stack 100h
.data
    msg db "Szabist $"
    
.code
main proc
     mov ax,@data
     mov ds,ax 
     mov cx,10  ; counter of 10
     
     print:
     mov ah,09h ; printing string
     int 21h
     loop print
     mov ah, 4ch ;used to end code
     
   
main endp 
end