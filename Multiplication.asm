.model small
.stack 100h
.data
msg1 db "Enter the value:$"
msg2 db "Answer:"
newline db 10,13,"$"

val1 db ?
val2 db ?

.code
main proc
    
    mov ax,@data
    mov ds,ax
    
    mov dx,offset msg1
    mov ah,9
    int 21h
    
    mov ah,1
    int 21h
    sub al,48 
    
    ;to edit variable from register
    mov si,offset val1
    mov ds:[si],al
    
    mov dx,offset newline
    mov ah,9
    int 21h 
    
    mov dx,offset msg1
    mov ah,9
    int 21h
    
    mov ah,1
    int 21h
    sub al,48 
    
    ;to edit variable from register
    mov si,offset val2
    mov ds:[si],al 
    
    mov dx,offset newline
    mov ah,9
    int 21h
    
    mov dx,offset msg2
    mov ah,9
    int 21h
    
    mov al,val1
    mov ah,00h
    
    mul val2
    
    mov ah,2
    mov dl,al
    add dl,48
    int 21h
       
    mov ah,4ch
    int 21h
    main endp
end main



