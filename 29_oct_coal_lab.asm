.model small
.stack 100h
.data
.code
main proc
    mov dl,'s'
    mov ah, 2       ;for printing single character 
    int 21h   ;interrupt function to interrupt all the work and solve the above problem     
    mov ah,1
    mov dl,ah
    int 21h
    main endp
end                                             