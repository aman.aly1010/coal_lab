.model small
.stack 100h
.data
    msg db "Szabist $"
    
.code
main proc
     mov ax,@data
     mov ds,ax 
     mov cx,10  ; counter of 10
     
     print:
     mov dx, offset msg
     mov ah,09h ; printing string
     int 21h
     
     ;new line
     mov dx,10
     mov ah,02h  
     int 21h
     
     ; carraige return
     mov dx,13
     mov ah,02h
     int 21h
     
     loop print
     mov ah, 4ch ;used to end code
     
   
main endp 
end





