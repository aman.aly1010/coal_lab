.model small
.stack 100h
.data
   msg1 db " Enter the Value: $ "
   msg2 db " Answer= $"
.code
main proc
   
    mov ax,@data 
    mov ds,ax 
    mov dx,offset msg1
    mov ah,09h
    int 21h      ; display msg
  
    mov ah,01h
    int 21h
    sub al,30h
    mov bl,al    ;ascii to int  
    
    mov dx,10   ; line change
    mov ah,02
    int 21h
    
    mov dx,13   ; carraige return
    mov ah,02
    int 21h
    
    mov ax,@data 
    mov ds,ax 
    mov dx,offset msg1
    mov ah,09h
    int 21h
    
    mov ah,01h
    int 21h
    sub al,30h 
    
    
    mul bl        ; result would be stored in al
    add al,30h
    mov ah,02h
    mov dl,al 
    int 21h  
    
    mov ah,4ch
    int 21h
    
main endp
end main

